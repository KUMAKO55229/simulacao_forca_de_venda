import 'dart:async';

import 'package:franck_app/models/PropostaSimulado/propostaSimulado.dart';
import 'package:franck_app/database/db-helper.dart';
import 'package:sqflite/sqflite.dart';
import '../controller.dart';

// Data Access Object
class  PropostaDAO {
  final controller = Controller();

  Future<Database> get db => DatabaseHelper.getInstance().db;

  Future<int> save(PropostaSimulado  propostaSimulado) async {
    var dbClient = await db;
    var id = await dbClient.insert("propostasimulado", controller.propostaSimulado.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    print('idProposta criada : $id');
    return id;
  }

  Future<List<PropostaSimulado>> findAll() async {
    final dbClient = await db;

    final list = await dbClient.rawQuery('select * from propostasimulado');

    final propostas = list.map<PropostaSimulado >((json) =>      controller.propostaSimulado.fromJson(json)).toList();
   // print("Proposta gravada : $propostas");
    print("lista gravada : $list");
    propostas.forEach((f) => print("Proposta gravada : $f"));
    return propostas;
  }

 /* Future<List<PropostaSimulado>> findAllByTipo(String tipo) async {
    final dbClient = await db;

    final list = await dbClient.rawQuery('select * from carro where tipo =? ',[tipo]);

    final carros = list.map<Carro>((json) => Carro.fromJson(json)).toList();

    return carros;
  }*/

/*  Future<Carro> findById(int id) async {
    var dbClient = await db;
    final list =
    await dbClient.rawQuery('select * from carro where id = ?', [id]);

    if (list.length > 0) {
      return new Carro.fromJson(list.first);
    }

    return null;
  }*/
/*
  Future<bool> exists(Carro carro) async {
    Carro c = await findById(carro.id);
    var exists = c != null;
    return exists;
  }

  Future<int> count() async {
    final dbClient = await db;
    final list = await dbClient.rawQuery('select count(*) from propostasimulado');
    return Sqflite.firstIntValue(list);
  }

  Future<int> delete(int id) async {
    var dbClient = await db;
    return await dbClient.rawDelete('delete from propostasimulado where id = ?', [id]);
  }*/

  Future<int> deleteAll() async {
    var dbClient = await db;
    return await dbClient.rawDelete('delete from propostasimulado');
  }
}
