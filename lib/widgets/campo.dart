import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
class Campo extends StatelessWidget {
 final String Title ;
 final double Mywidth ;
 final Function onchanged1 ;

  final  String  errorText ;

     Campo(this.Title, this.Mywidth, {this.onchanged1, this.errorText}) ;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(

      children: <Widget>[

        Text(Title),

        Container(
            width: Mywidth ,
            margin: EdgeInsets.all(10.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                left: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                right: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                bottom: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
              ),
            ),
            child: TextField(
              autofocus: true,
              decoration: InputDecoration(
                border: InputBorder.none,
             //     errorText:  errorText == null ? null : errorText();
              ),
              onChanged:  onchanged1  ,

            ),
        ),

      ],
    );
  }


}