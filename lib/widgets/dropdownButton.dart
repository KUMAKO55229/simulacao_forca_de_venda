import"package:flutter/material.dart" ;

class DropdownButton1 extends StatefulWidget {
  final Function onchanged3;

  const DropdownButton1({Key key, this.onchanged3}) : super(key: key);
  @override
  __DropdownButtonState createState()  => __DropdownButtonState() ;
}

class __DropdownButtonState  extends State<DropdownButton1> {
  String dropdownValue =   'Selecionne';



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DropdownButton<String>(
      value: dropdownValue,
      //  disabledHint: Text("Selecionne"),
      icon: Icon(Icons.arrow_downward),
      iconSize: 24,
      elevation: 16,
      style: TextStyle(color: Colors.black54 , fontWeight: FontWeight.bold),
      underline: Container(
        height: 2,
        color: Colors.transparent,
      ),
      onChanged: (String newValue) {
        widget.onchanged3;
        setState(() {
          dropdownValue = newValue;
        });
      },

      items: <String>['Selecionne','Concorrente 1', 'Concorrente 2', 'Concorrente 3', 'Concorrente 4']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child:Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
             // Icon(Icons.business),
              SizedBox(width: 170),
              Text(value),
            ],
          ) , // Text(value),

        );
      }).toList(),
    );
  }
}