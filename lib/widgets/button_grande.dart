import 'package:flutter/material.dart';
import 'package:franck_app/pages/simulador/simulador.dart';
import 'package:franck_app/utils/nav.dart';
import 'package:franck_app/pages/simulador/proposta_aceita.dart';
import 'package:provider/provider.dart';

class ButtonGrande extends StatelessWidget {
  final Text text2;
  final Color Color2;
  final EdgeInsetsGeometry Margin;

  final StatefulWidget next2;


 // final Function func;

  ButtonGrande(this.text2, this.Color2, this.Margin, {this.next2, /*, this.func*/});
  @override
  Widget build(BuildContext context) {
    // final propsta = Provider.of<Proposta>(context);
    // TODO: implement build
    return Container(
      width: MediaQuery.of(context).size.width,
      color: Color2,
      margin: Margin,
      child:
          FlatButton(child: text2, onPressed: () => _onClickSimuacao(context)),
    );
  }

  _onClickSimuacao(BuildContext context) {

    if (next2 != null) {
      push(context, next2);
    } else {
      return;
    }
    //push(context, next2);
  }
}
