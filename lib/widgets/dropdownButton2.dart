import"package:flutter/material.dart" ;

class DropdownButton2 extends StatefulWidget {
 final Function onchanged2;

  const DropdownButton2({Key key, this.onchanged2}) : super(key: key);

  @override
  __DropdownButton2State createState()  => __DropdownButton2State() ;
}

class __DropdownButton2State  extends State<DropdownButton2> {
  String dropdownValue =   'Selecionne';









  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DropdownButton<String>(
      value: dropdownValue,
      //  disabledHint: Text("Selecionne"),
      icon: Icon(Icons.arrow_downward),
      iconSize: 24,
      elevation: 16,
      style: TextStyle(color: Colors.black54 , fontWeight: FontWeight.bold),
      underline: Container(
        height: 2,
        color: Colors.transparent,
      ),
      onChanged: (String newValue) {
        widget.onchanged2;
        setState(() {
          dropdownValue = newValue;
        });
      },

      items: <String>['Selecionne','Ramo 1', 'Ramo 2', 'Ramo 3', 'Ramo 4']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child:Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              //Icon(Icons.branding_watermark),
              SizedBox(width: 170),
              Text(value),
            ],
          ) , // Text(value),

        );
      }).toList(),
    );
  }
}