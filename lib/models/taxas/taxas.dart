import 'package:mobx/mobx.dart';

// Include generated file
part 'taxas.g.dart';

// This is the class used by rest of your codebase
class Taxas = _Taxas with _$Taxas;

// The store-class
abstract class _Taxas with Store {
  @observable
  String ramoAtividade;

  @action
  mudouRamoAtividade(String value) => ramoAtividade = value;

  @observable
  double taxaMinima;

  @action
  mudouTaxaMinimae(double value) => taxaMinima = value;

  @observable
  int idTaxa;

  @action
  mudouidTaxa(int value) => idTaxa = value;
}
