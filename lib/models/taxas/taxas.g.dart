// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'taxas.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Taxas on _Taxas, Store {
  final _$ramoAtividadeAtom = Atom(name: '_Taxas.ramoAtividade');

  @override
  String get ramoAtividade {
    _$ramoAtividadeAtom.context.enforceReadPolicy(_$ramoAtividadeAtom);
    _$ramoAtividadeAtom.reportObserved();
    return super.ramoAtividade;
  }

  @override
  set ramoAtividade(String value) {
    _$ramoAtividadeAtom.context.conditionallyRunInAction(() {
      super.ramoAtividade = value;
      _$ramoAtividadeAtom.reportChanged();
    }, _$ramoAtividadeAtom, name: '${_$ramoAtividadeAtom.name}_set');
  }

  final _$taxaMinimaAtom = Atom(name: '_Taxas.taxaMinima');

  @override
  double get taxaMinima {
    _$taxaMinimaAtom.context.enforceReadPolicy(_$taxaMinimaAtom);
    _$taxaMinimaAtom.reportObserved();
    return super.taxaMinima;
  }

  @override
  set taxaMinima(double value) {
    _$taxaMinimaAtom.context.conditionallyRunInAction(() {
      super.taxaMinima = value;
      _$taxaMinimaAtom.reportChanged();
    }, _$taxaMinimaAtom, name: '${_$taxaMinimaAtom.name}_set');
  }

  final _$idTaxaAtom = Atom(name: '_Taxas.idTaxa');

  @override
  int get idTaxa {
    _$idTaxaAtom.context.enforceReadPolicy(_$idTaxaAtom);
    _$idTaxaAtom.reportObserved();
    return super.idTaxa;
  }

  @override
  set idTaxa(int value) {
    _$idTaxaAtom.context.conditionallyRunInAction(() {
      super.idTaxa = value;
      _$idTaxaAtom.reportChanged();
    }, _$idTaxaAtom, name: '${_$idTaxaAtom.name}_set');
  }

  final _$_TaxasActionController = ActionController(name: '_Taxas');

  @override
  dynamic mudouRamoAtividade(String value) {
    final _$actionInfo = _$_TaxasActionController.startAction();
    try {
      return super.mudouRamoAtividade(value);
    } finally {
      _$_TaxasActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic mudouTaxaMinimae(double value) {
    final _$actionInfo = _$_TaxasActionController.startAction();
    try {
      return super.mudouTaxaMinimae(value);
    } finally {
      _$_TaxasActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic mudouidTaxa(int value) {
    final _$actionInfo = _$_TaxasActionController.startAction();
    try {
      return super.mudouidTaxa(value);
    } finally {
      _$_TaxasActionController.endAction(_$actionInfo);
    }
  }
}
