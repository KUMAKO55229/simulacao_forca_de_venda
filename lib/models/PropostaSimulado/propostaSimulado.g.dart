// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'propostaSimulado.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PropostaSimulado on _PropostaSimulado, Store {
  final _$concorrenteAtom = Atom(name: '_PropostaSimulado.concorrente');

  @override
  String get concorrente {
    _$concorrenteAtom.context.enforceReadPolicy(_$concorrenteAtom);
    _$concorrenteAtom.reportObserved();
    return super.concorrente;
  }

  @override
  set concorrente(String value) {
    _$concorrenteAtom.context.conditionallyRunInAction(() {
      super.concorrente = value;
      _$concorrenteAtom.reportChanged();
    }, _$concorrenteAtom, name: '${_$concorrenteAtom.name}_set');
  }

  final _$cpfAtom = Atom(name: '_PropostaSimulado.cpf');

  @override
  String get cpf {
    _$cpfAtom.context.enforceReadPolicy(_$cpfAtom);
    _$cpfAtom.reportObserved();
    return super.cpf;
  }

  @override
  set cpf(String value) {
    _$cpfAtom.context.conditionallyRunInAction(() {
      super.cpf = value;
      _$cpfAtom.reportChanged();
    }, _$cpfAtom, name: '${_$cpfAtom.name}_set');
  }

  final _$telefoneAtom = Atom(name: '_PropostaSimulado.telefone');

  @override
  String get telefone {
    _$telefoneAtom.context.enforceReadPolicy(_$telefoneAtom);
    _$telefoneAtom.reportObserved();
    return super.telefone;
  }

  @override
  set telefone(String value) {
    _$telefoneAtom.context.conditionallyRunInAction(() {
      super.telefone = value;
      _$telefoneAtom.reportChanged();
    }, _$telefoneAtom, name: '${_$telefoneAtom.name}_set');
  }

  final _$emailAtom = Atom(name: '_PropostaSimulado.email');

  @override
  String get email {
    _$emailAtom.context.enforceReadPolicy(_$emailAtom);
    _$emailAtom.reportObserved();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.context.conditionallyRunInAction(() {
      super.email = value;
      _$emailAtom.reportChanged();
    }, _$emailAtom, name: '${_$emailAtom.name}_set');
  }

  final _$ramoAtividadeAtom = Atom(name: '_PropostaSimulado.ramoAtividade');

  @override
  String get ramoAtividade {
    _$ramoAtividadeAtom.context.enforceReadPolicy(_$ramoAtividadeAtom);
    _$ramoAtividadeAtom.reportObserved();
    return super.ramoAtividade;
  }

  @override
  set ramoAtividade(String value) {
    _$ramoAtividadeAtom.context.conditionallyRunInAction(() {
      super.ramoAtividade = value;
      _$ramoAtividadeAtom.reportChanged();
    }, _$ramoAtividadeAtom, name: '${_$ramoAtividadeAtom.name}_set');
  }

  final _$debitoConcorrenteAtom =
      Atom(name: '_PropostaSimulado.debitoConcorrente');

  @override
  String get debitoConcorrente {
    _$debitoConcorrenteAtom.context.enforceReadPolicy(_$debitoConcorrenteAtom);
    _$debitoConcorrenteAtom.reportObserved();
    return super.debitoConcorrente;
  }

  @override
  set debitoConcorrente(String value) {
    _$debitoConcorrenteAtom.context.conditionallyRunInAction(() {
      super.debitoConcorrente = value;
      _$debitoConcorrenteAtom.reportChanged();
    }, _$debitoConcorrenteAtom, name: '${_$debitoConcorrenteAtom.name}_set');
  }

  final _$descontoDebitoAtom = Atom(name: '_PropostaSimulado.descontoDebito');

  @override
  String get descontoDebito {
    _$descontoDebitoAtom.context.enforceReadPolicy(_$descontoDebitoAtom);
    _$descontoDebitoAtom.reportObserved();
    return super.descontoDebito;
  }

  @override
  set descontoDebito(String value) {
    _$descontoDebitoAtom.context.conditionallyRunInAction(() {
      super.descontoDebito = value;
      _$descontoDebitoAtom.reportChanged();
    }, _$descontoDebitoAtom, name: '${_$descontoDebitoAtom.name}_set');
  }

  final _$creditoConcorrenteAtom =
      Atom(name: '_PropostaSimulado.creditoConcorrente');

  @override
  String get creditoConcorrente {
    _$creditoConcorrenteAtom.context
        .enforceReadPolicy(_$creditoConcorrenteAtom);
    _$creditoConcorrenteAtom.reportObserved();
    return super.creditoConcorrente;
  }

  @override
  set creditoConcorrente(String value) {
    _$creditoConcorrenteAtom.context.conditionallyRunInAction(() {
      super.creditoConcorrente = value;
      _$creditoConcorrenteAtom.reportChanged();
    }, _$creditoConcorrenteAtom, name: '${_$creditoConcorrenteAtom.name}_set');
  }

  final _$descontoCreditoAtom = Atom(name: '_PropostaSimulado.descontoCredito');

  @override
  String get descontoCredito {
    _$descontoCreditoAtom.context.enforceReadPolicy(_$descontoCreditoAtom);
    _$descontoCreditoAtom.reportObserved();
    return super.descontoCredito;
  }

  @override
  set descontoCredito(String value) {
    _$descontoCreditoAtom.context.conditionallyRunInAction(() {
      super.descontoCredito = value;
      _$descontoCreditoAtom.reportChanged();
    }, _$descontoCreditoAtom, name: '${_$descontoCreditoAtom.name}_set');
  }

  final _$_PropostaSimuladoActionController =
      ActionController(name: '_PropostaSimulado');

  @override
  dynamic mudouConcorrente(String value) {
    final _$actionInfo = _$_PropostaSimuladoActionController.startAction();
    try {
      return super.mudouConcorrente(value);
    } finally {
      _$_PropostaSimuladoActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic mudouCPF(String value) {
    final _$actionInfo = _$_PropostaSimuladoActionController.startAction();
    try {
      return super.mudouCPF(value);
    } finally {
      _$_PropostaSimuladoActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic mudouTelefone(String value) {
    final _$actionInfo = _$_PropostaSimuladoActionController.startAction();
    try {
      return super.mudouTelefone(value);
    } finally {
      _$_PropostaSimuladoActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic mudouEmail(String value) {
    final _$actionInfo = _$_PropostaSimuladoActionController.startAction();
    try {
      return super.mudouEmail(value);
    } finally {
      _$_PropostaSimuladoActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic mudouRamoAtividade(String value) {
    final _$actionInfo = _$_PropostaSimuladoActionController.startAction();
    try {
      return super.mudouRamoAtividade(value);
    } finally {
      _$_PropostaSimuladoActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic mudouDebitoConcorrente(String value) {
    final _$actionInfo = _$_PropostaSimuladoActionController.startAction();
    try {
      return super.mudouDebitoConcorrente(value);
    } finally {
      _$_PropostaSimuladoActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic mudouDescontoDebito(String value) {
    final _$actionInfo = _$_PropostaSimuladoActionController.startAction();
    try {
      return super.mudouDescontoDebito(value);
    } finally {
      _$_PropostaSimuladoActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic mudouCreditoConcorrente(String value) {
    final _$actionInfo = _$_PropostaSimuladoActionController.startAction();
    try {
      return super.mudouCreditoConcorrente(value);
    } finally {
      _$_PropostaSimuladoActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic mudouDescontoCredito(String value) {
    final _$actionInfo = _$_PropostaSimuladoActionController.startAction();
    try {
      return super.mudouDescontoCredito(value);
    } finally {
      _$_PropostaSimuladoActionController.endAction(_$actionInfo);
    }
  }
}
