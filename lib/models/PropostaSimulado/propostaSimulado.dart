import 'package:mobx/mobx.dart';

// Include generated file
part 'propostaSimulado.g.dart';

// This is the class used by rest of your codebase
class PropostaSimulado = _PropostaSimulado with _$PropostaSimulado;

// The store-class
abstract class _PropostaSimulado with Store {
  @observable
  String concorrente;

  @action
  mudouConcorrente(String value) => concorrente = value;

  @observable
  String cpf;

  @action
  mudouCPF(String value) => cpf = value;

  @observable
  String telefone;
  @action
  mudouTelefone(String value) => telefone = value;

  @observable
  String email;

  @action
  mudouEmail(String value) => email = value;

  @observable
  String ramoAtividade;

  @action
  mudouRamoAtividade(String value) => ramoAtividade = value;

  @observable
  String debitoConcorrente;

  @action
  mudouDebitoConcorrente(String value) => debitoConcorrente = value;

  @observable
  String descontoDebito;

  @action
  mudouDescontoDebito(String value) => descontoDebito = value;

  @observable
  String creditoConcorrente;

  @action
  mudouCreditoConcorrente(String value) => creditoConcorrente = value;

  @observable
  String descontoCredito;

  @action
  mudouDescontoCredito(String value) => descontoCredito = value;

  @observable
  int idProposta;

  @action
  mudouIdProposta(int value) => idProposta = value;

  @action
  fromJson(Map<String, dynamic> json) {
    idProposta = json['idProposta'];
    concorrente = json['concorrente'];
    cpf = json['cpf'];
    telefone = json['telefone'];
    email = json['email'];
    ramoAtividade = json['ramoAtividade'];
    debitoConcorrente = json['debitoConcorrente'];

    descontoDebito = json[descontoDebito];
    creditoConcorrente = json[descontoDebito];
    descontoCredito = json[descontoDebito];
  }

  @action
  Map<String, dynamic> toJson() => {
        "idProposta": idProposta,
        "concorrente": concorrente,
        "cpf": cpf,
        "telefone": telefone,
        "email": email,
        "ramoAtividade": ramoAtividade,
        "debitoConcorrente": debitoConcorrente,
        "descontoDebito": descontoDebito,
        "creditoConcorrente": creditoConcorrente,
        "descontoCredito": descontoCredito,
      };
}
