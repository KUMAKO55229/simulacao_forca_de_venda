import 'package:flutter/material.dart';
import 'package:franck_app/pages/simulador/simulador2.dart';
import 'package:franck_app/pages/simulador/taxtas_forms.dart';
import 'package:franck_app/utils/nav.dart';
import 'package:franck_app/widgets/button_grande.dart';

//import '../../controller.dart';
import 'cliente_form.dart';

class Simulador extends StatefulWidget {
  @override
  _SimuladorState createState() => _SimuladorState();
}

class _SimuladorState extends State<Simulador> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Simulador"),
          backgroundColor: Colors.white70,
        ),
        body: ListView(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only( top:
                10.0, bottom: 10.0 , left : 53.0
              ),
              child: Text(
                "Dados do cliente",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: EdgeInsets.only( top:
              10.0, bottom: 10.0 , left : 53.0
              ),
              child: Text(
                "Os campos obrigatorios estão sinalizados com *",
                style: TextStyle(fontSize: 16.0),
              ),
            ),
            ClienteForm(),
            Container(
              margin: EdgeInsets.only(top: MediaQuery.of(context).size.height - 513),
              color: Colors.black54,
              child:  ListTile(

                title: ButtonGrande( Text("Próximo",
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white)
                ),
                    Colors.transparent,
                    EdgeInsets.all(0.0),
               next2 : Simulador2(),
               ),
                trailing: Icon(Icons.navigate_next, color: Colors.white,),
                onTap: ()=>__proximo(context),

              ),
            )
          ],
        ));
  }

     __proximo(BuildContext context) {
     push(context, Simulador2());
  }
}
