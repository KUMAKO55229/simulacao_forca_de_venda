import "package:flutter/material.dart";
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:franck_app/widgets/campo.dart';
import 'package:franck_app/widgets/dropdownButton.dart';
import 'package:franck_app/widgets/dropdownButton2.dart';

import '../../controller.dart';

class ClienteForm extends StatefulWidget {
  @override
  _ClienteFormState createState() => _ClienteFormState();
}

class _ClienteFormState extends State<ClienteForm> {
  final controller = Controller();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Form(
        child: Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Observer(
            builder: (_) {
              return Campo("CPF*", MediaQuery.of(context).size.width / 3,
                  onchanged1: controller.propostaSimulado.mudouCPF);
            },
          ),
          Observer(
            builder: (_) {
              return Campo("Telefone*", MediaQuery.of(context).size.width / 3,
                  onchanged1: controller.propostaSimulado.mudouTelefone);
            },
          )
        ],
      ),
      Observer(
        builder: (_) {
          return Campo("Email", MediaQuery.of(context).size.width * 5 / 7,
              onchanged1: controller.propostaSimulado.mudouEmail);
        },
      ),

      //   Campo("Ramo de Atividade *", MediaQuery.of(context).size.width * 5 / 7),

      Column(
        children: <Widget>[
          Text("Ramo de Atividade *"),
          Container(
              width: MediaQuery.of(context).size.width * 5 / 7,
              margin: EdgeInsets.all(10.0),
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                  left: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                  right: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                  bottom: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                ),
              ),
              child: Observer(
                builder: (_) {
                  return DropdownButton2(
                      onchanged2:
                      controller.propostaSimulado.mudouRamoAtividade
                  );
                },
              )),
        ],
      )
    ]));
  }
}
