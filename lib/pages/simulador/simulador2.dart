import 'package:flutter/material.dart';
import 'package:franck_app/database/db-helper.dart';
import 'package:franck_app/database/proposta-simulado-dao.dart';
import 'package:franck_app/pages/simulador/proposta_aceita.dart';
import 'package:franck_app/pages/simulador/taxtas_forms.dart';
import 'package:franck_app/utils/nav.dart';
import 'package:franck_app/widgets/button_grande.dart';
import 'package:franck_app/widgets/dropdownButton.dart';

import '../../controller.dart';
import 'cliente_form.dart';

class Simulador2 extends StatefulWidget {
  @override
  _Simulador2State createState() => _Simulador2State();
}

class _Simulador2State extends State<Simulador2> {
  final controller = Controller();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Simulador "),
          backgroundColor: Colors.white70,
        ),
        body: ListView(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 33.0),
              child: Text(
                "Informacões de taxa",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 33.0),
              child: Text(
                "Os campos obrigatorios estão sinalizados com *",
                style: TextStyle(fontSize: 16.0),
              ),
            ),
            TaxasForm(),
            Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height - 545),
              color: Colors.black54,
              child: ListTile(
                title: ButtonGrande(
                    Text("Simular",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white)),
                    Colors.transparent,
                    EdgeInsets.all(0.0),
                    next2 : Proposta(),
                 // func: _simular(context),
                   ),
                trailing: Icon(
                  Icons.navigate_next,
                  color: Colors.white,
                ),
                onTap: () => _simular(context),
              ),
            )
          ],
        ));
  }

  _simular(BuildContext context) {

    var propostaDAO =   PropostaDAO();
    print("Gravando no banco de dados");
    propostaDAO.save(controller.propostaSimulado);
    print("Terminou de gravar no banco de dados");
    var propostaGravada = propostaDAO.findAll();
    push(context, Proposta());
  }
}
