import "package:flutter/material.dart";
import 'package:franck_app/pages/home/home_page.dart';
import 'package:franck_app/pages/simulador/simulador.dart';
import 'package:franck_app/pages/simulador/simulador2.dart';
import 'package:franck_app/utils/nav.dart';
import 'package:franck_app/widgets/button_grande.dart';

class Proposta extends StatefulWidget {
  @override
  _PropostaState createState() => _PropostaState();
}

class _PropostaState extends State<Proposta> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Simulador"),
        backgroundColor: Colors.white70,
      ),
      body: ListView(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 53.0),
            child: Text(
              "Taxa da Simulação permitidas",
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 53.0),
            child: _permitidaOuNao(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 53.0),
                child: Text(
                  "Tipo",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                "Concorrente",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              ),
              Container(
                margin: EdgeInsets.only(right: 53.0),
                child: Text(
                  "Proposta",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(left: 53.0, right: 53.0),
            child: Divider(
              thickness: 2.0,
              color: Colors.black54,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 53.0),
                child: Text(
                  "Débito",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                "3,5%",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              ),
              Container(
                margin: EdgeInsets.only(right: 53.0),
                child: Text(
                  "1,5%",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(left: 53.0, right: 53.0),
            child: Divider(
              thickness: 2.0,
              color: Colors.black54,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 53.0),
                child: Text(
                  "Crédito",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                "3,5%",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              ),
              Container(
                margin: EdgeInsets.only(right: 53.0),
                child: Text(
                  "1,5%",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(left: 53.0, right: 53.0),
            child: Divider(
              thickness: 2.0,
              color: Colors.black54,
            ),
          ),
          Container(
            child: _AceitarOuReajustar(),
          )
        ],
      ),
    );
  }

  _permitidaOuNao() {
    var x = true;
    if (x) {
      return Text("Todos as taxas são permitidas. \n ofereça ao seu cliente",
          style: TextStyle(fontSize: 16.0));
    } else
      return Text(
          "A (s) taxa(s) simulada(s) não foi fo(ram) permitida(s) pois \n, não atinge(m) o mínimo requisitado.",
          style: TextStyle(fontSize: 16.0));
  }

  _AceitarOuReajustar() {
    var y = true;

    if (y) {
      return Column(
        children: <Widget>[
          Container(
            margin:
                EdgeInsets.only(top: MediaQuery.of(context).size.height - 420),
            color: Colors.black54,
            child: ListTile(
              title: ButtonGrande(
                  Text("Proposta aceita",
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.white)),
                  Colors.transparent,
                  EdgeInsets.all(0.0),

              ),
              trailing: Icon(
                Icons.navigate_next,
                color: Colors.white,
              ),
              onTap: () => _proposta_aceita(context),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 0.0),
            // color: Colors.white70,
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                left: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                right: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                bottom: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
              ),
              //  color: Color(0xFFFF000000),
            ),
            child: ListTile(
              title: ButtonGrande(
                  Text("Recursar",
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.black54)
                  ),

                  Colors.transparent,
                  EdgeInsets.all(0.0) ,
               // func: _proposta_aceita(context),
              ),
              onTap: () => _proposta_aceita(context),
            ),
          )
        ],
      );
    } else
      return Container(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                  top: 10.0, bottom: 10.0, left: 53.0, right: 53.0),
              child: Text(
                  'O valor de taxa mínima para débito é 2,0%, enquanto para  crédito é 3,0% ',
                  style: TextStyle(fontSize: 16.0)),
            ),
            Container(
              margin: EdgeInsets.only(top: 0.0),
              // color: Colors.white70,
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                  left: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                  right: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                  bottom: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                ),
                color: Color(0x8A000000),
              ),
              child: ListTile(
                title: ButtonGrande(
                    Text("Reajustar",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white)),
                    Colors.transparent,
                    EdgeInsets.all(0.0)

                ),
                onTap: () => _reajustar(context),
              ),
            )
          ],
        ),
      );
  }

  _reajustar(BuildContext context) {
    pop(context);
    // pushReplacement(context, Simulador2()) ;
    // push(context , Simulador2());
  }

  _proposta_aceita(BuildContext context) {
    //configura o AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Aceite do cliente"),
      content: Text("Resposta gravada no banco de dados "),
      actions: [
        ButtonGrande(
            Text("Voltar para inicio",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white)),
            Colors.black54,
            EdgeInsets.all(33.0) ,
        next2: HomePage(),)
      ],
    );

    //exibe o diálogo
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
