import "package:flutter/material.dart";
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:franck_app/widgets/campo.dart';
import 'package:franck_app/widgets/dropdownButton.dart';

import '../../controller.dart';
class TaxasForm extends StatefulWidget {
  @override
  _TaxasFormState createState() => _TaxasFormState();
}

class _TaxasFormState extends State<TaxasForm> {
  final controller = Controller();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Form(
        child: Column(children: [
          Container(
            width:MediaQuery.of(context).size.width -66 ,
            margin : EdgeInsets.all(10.0),
            padding: EdgeInsets.only(left: 10.0),
            decoration: const BoxDecoration(

              border: Border(
                top: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                left: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                right: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                bottom: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
              ),
            ),
            child: Observer( builder :(_) {
                   return  DropdownButton1( onchanged3: controller.propostaSimulado.mudouConcorrente) ;
            } )
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[

              Container(
                margin: EdgeInsets.only(left: 33.0),
                child: Text("Débito" , style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold) ),
              )
            ],
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[

             Observer(
               builder: (_){
                 return  Campo("Taxa do concorrente*", MediaQuery.of(context).size.width / 3 + 22.0, onchanged1: controller.propostaSimulado.mudouDebitoConcorrente);
               },
             ),

              Observer(
                builder: (_){
                  return  Campo("Desconto oferecido*", MediaQuery.of(context).size.width / 3 + 32.0 , onchanged1: controller.propostaSimulado.mudouDescontoDebito,);
                }
              ),

            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[

             Container(
               margin: EdgeInsets.only(left: 33.0),
               child:  Text("Crédito" , style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold) ),
             )
            ],
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[

              Observer(
                builder: (_){
                  return Campo("Taxa do concorrente*", MediaQuery.of(context).size.width / 3 + 22.0 , onchanged1: controller.propostaSimulado.mudouCreditoConcorrente,) ;
                },
              ),
               Observer(
                 builder: (_){
                   return  Campo("Desconto oferecido*", MediaQuery.of(context).size.width / 3 + 32.0 , onchanged1: controller.propostaSimulado.mudouDescontoCredito,);
                 },
               )
            ],
          ),
        ]));


  }
}


/*    Container(
            child:  ListTile(
              title: TextField(
                decoration: const InputDecoration(  border: InputBorder.none,),

              ),
            ),
          ),


 */