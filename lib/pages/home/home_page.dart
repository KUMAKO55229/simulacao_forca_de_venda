import 'package:flutter/material.dart';
import 'package:franck_app/pages/simulador/simulador.dart';
import 'package:franck_app/pages/simulador/visualizar_proposta.dart';

import 'package:franck_app/utils/nav.dart';
import 'package:franck_app/widgets/button_grande.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Simulação"),
        backgroundColor: Colors.white70,
      ),
      body: Column(
        children: <Widget>[
          ButtonGrande(
              Text("Nova simulação ",
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white)
              ),
              Colors.black54,
              EdgeInsets.all(33.0) ,
              next2 : Simulador()
          ),
          Container(
            margin: EdgeInsets.only(left: 33.0, right: 33.0, bottom: 33.0 , top: 0.0),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                left: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                right: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                bottom: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
              ),
              color: Color(0xFFFF000000),
            ),
            child: ButtonGrande(
                Text(
                  "Visualizar propostas aceitas",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Colors.white,
                EdgeInsets.all(0.0),
              next2:  Visualizar()
            ),

          ),


        ],
      ),
    );
  }
}
