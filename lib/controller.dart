import 'package:mobx/mobx.dart';

import 'models/PropostaSimulado/propostaSimulado.dart';
import 'models/taxas/taxas.dart';

// Include generated file
part 'controller.g.dart';

// This is the class used by rest of your codebase
class Controller = _Controller with _$Controller;

// The store-class
abstract class _Controller with Store {
  var propostaSimulado = PropostaSimulado() ;
  var taxas  = Taxas();


  String validateCpf() {
     if(propostaSimulado.cpf == null || propostaSimulado.cpf.isEmpty){
       return "esse campo é obrigatorio " ;

     }
     return null ;
  }




}