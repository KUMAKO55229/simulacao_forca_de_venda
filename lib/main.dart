import 'package:flutter/material.dart';
import 'package:franck_app/pages/home/home_page.dart' ;
import 'package:franck_app/pages/simulador/proposta_aceita.dart';
import 'package:provider/provider.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider (
         providers: [
           Provider<Proposta>(create: (_)=> Proposta()),
         ],

      child:  MaterialApp(
        title: 'Avaliação Técnica Vek ',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(

          primaryColor : Colors.white,
        ),
        home:   HomePage () ,
      ),
    );
  }
}
